﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimation : MonoBehaviour
{
    public LayerMask AttackLayer;

    [SerializeField] private Animator ani;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position + Vector3.up * 1.15167f, transform.forward * 2.0f, Color.cyan);
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Debug.Log("kicking!");
            ani.SetTrigger("isToKick");

            if (Physics.SphereCast(origin: transform.position + Vector3.up * 1.15167f,
                radius: 0.5f,
                direction: transform.forward,
                hitInfo: out RaycastHit hitinfo,
                maxDistance: 2.0f,
                layerMask: AttackLayer))
            {
                Debug.Log("kicked sth! " + hitinfo.collider.tag);
                if (hitinfo.collider.tag == "Enemy")
                {
                    hitinfo.collider.GetComponent<EnemyHitPointComponent>().TakeDamage();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            ani.Play("attack_throw",0);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            ani.Play("toDance",0);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            ani.SetTrigger("isToKick_x3");
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            ani.SetTrigger("isToKick");
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            ani.SetBool("isDead",!ani.GetBool("isDead"));
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            var randomHitAnimation = Random.Range(1,4);
            var hitAnimationId = "isHit_1";
            switch(randomHitAnimation)
            {
                case 1:
                {
                    hitAnimationId = "isHit_1";
                    break;
                }
                case 2:
                {
                    hitAnimationId = "isHit_2";
                    break;
                }
                case 3:
                {
                    hitAnimationId = "isHit_3";
                    break;
                }
                case 4:
                {
                    hitAnimationId = "isHit_4";
                    break;
                }
            }
            ani.SetTrigger(hitAnimationId);
        }
    }
}
