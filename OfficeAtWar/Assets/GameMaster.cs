﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public static GameMaster GM;

    public GameObject PlayerCharacter 
    {
        get
        {
            return GameObject.FindGameObjectWithTag("Player");
        }
    }
    public int score;

    public Dictionary<string, MountingPoint> weapon_mounting_dict = new Dictionary<string, MountingPoint>() {
        // Availble Weapon List
        { "mesh_extinguisher_01", new MountingPoint(1, 0, 0, -90, 0, 90, 1) },
        { "mesh_plant_a_01", new MountingPoint(0, 0, 0, 0, 0, 90, 1) },
        { "mesh_furniture_chair_03", new MountingPoint(0, 0, 0, -90, 0, 0, 1) },
        { "mesh_toolbox_01", new MountingPoint(0, 0.08f, 0.02f, 0, 0, -180, 1) },
        { "clock", new MountingPoint(0.05f, 0.39f, 0, 0, 181.35f, 0.38f, 1.5f) },
        { "coatrack", new MountingPoint(0.59f, 0, 0.34f, 80, -118.4f, 0, 0.8f) },
        { "mousepad", new MountingPoint(-1.25f, 0.58f, -1.52f, -219.5f, -4.8f, 17.5f, 2.46f) },
        { "mug", new MountingPoint(0.19f, 0.32f, -0.06f, 81.57f, 0, -280, 3) },
        { "suitcase", new MountingPoint(0, 0, 0, -180, 0, 0, 2) },
    };

    public Dictionary<string, int> weapon_durability_dict = new Dictionary<string, int>() {
        // Availble Weapon List
        { "mesh_extinguisher_01", 20 }, 
        { "mesh_plant_a_01", 5 },
        { "mesh_furniture_chair_03", 50 },
        { "mesh_toolbox_01", 10 },
    };

    public Dictionary<string, GameObject> weapon_fbx_dict = new Dictionary<string, GameObject>();
    public Dictionary<string, GameObject> weapon_throwprefab_dict = new Dictionary<string, GameObject>();
    public Dictionary<GameObject, int> damaged_goods = new Dictionary<GameObject, int>();

    public GameObject mesh_extinguisher_01;
    public GameObject mesh_plant_a_01;
    public GameObject mesh_furniture_chair_03;
    public GameObject mesh_toolbox_01;
    public GameObject Throwable_item0_fireExtinguisher;
    public GameObject Throwable_item1_plantA;
    public GameObject Throwable_item3_chair;
    public GameObject Throwable_item2_toolBox;


    // Use OnValidate () instead of Awake () if you need the singleton in the editor. 
    void Awake()
    {
        weapon_fbx_dict = new Dictionary<string, GameObject>() {
            { "mesh_extinguisher_01", mesh_extinguisher_01 },
            { "mesh_plant_a_01", mesh_plant_a_01 },
            { "mesh_furniture_chair_03", mesh_furniture_chair_03 },
            { "mesh_toolbox_01", mesh_toolbox_01 },
        };

        // this string = sting in weapon_fbx_dict
        weapon_throwprefab_dict = new Dictionary<string, GameObject>() {
            { "mesh_extinguisher_01", Throwable_item0_fireExtinguisher },
            { "mesh_plant_a_01", Throwable_item1_plantA },
            { "mesh_furniture_chair_03", Throwable_item3_chair },
            { "mesh_toolbox_01", Throwable_item2_toolBox },
        };

        if (GM != null)
            Destroy(GM);
        else
            GM = this;

        DontDestroyOnLoad(this.gameObject);
    }
}
