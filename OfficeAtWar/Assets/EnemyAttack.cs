﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public LayerMask AttackLayer;
    [SerializeField] private float slopeForce;
    [SerializeField] private float slopeForceRayLength;

    private CharacterController charController;
    public Animator ani;
    private int PlayerHitPoint;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnemyAttackAnimation()
    {          
        player = GameObject.FindGameObjectWithTag("Player");
        PlayerHitPoint = player.GetComponent<PlayerHitPoint>().PlayerLife;
        if (PlayerHitPoint > 0)
        {
            ani.SetTrigger("isToKick");
            Debug.DrawRay(transform.position + Vector3.up * 1.15167f, transform.forward * 2.0f, Color.cyan);
        
            if (Physics.SphereCast(origin: transform.position + Vector3.up * 1.15167f,
                radius: 0.5f,
                direction: transform.forward,
                hitInfo: out RaycastHit hitinfo,
                maxDistance: 2.0f,
                layerMask: AttackLayer))
            {
                Debug.Log("Enemy kicked sth! " + hitinfo.collider.tag);
                if (hitinfo.collider.tag == "Player")
                {
                    hitinfo.collider.GetComponent<PlayerHitPoint>().TakeDamage();
                   //PlayerHitPoint = hitinfo.collider.GetComponent<PlayerHitPoint>().PlayerLife();
                }
            }
        }
    }
}
