﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using UnityEngine;


public class MountingPoint
{
    public float pos_x { get; set; }
    public float pos_y { get; set; }
    public float pos_z { get; set; }
    public float rot_x { get; set; }
    public float rot_y { get; set; }
    public float rot_z { get; set; }
    public float itemSize { get; set; }

    public MountingPoint(float PosX, float PosY, float PosZ, float RotX, float RotY, float RotZ, float size)
    {
        pos_x = PosX;
        pos_y = PosY;
        pos_z = PosZ;
        rot_x = RotX;
        rot_y = RotY;
        rot_z = RotZ;
        itemSize = size;
    }
}

public class ItemsOnHand : MonoBehaviour
{
    public GameObject MainWeaponAttachment;
    private GameObject WeaponSlot1;

    public bool isDebug = false;
    public float position_x;
    public float position_y;
    public float position_z;
    public float rotation_x;
    public float rotation_y;
    public float rotation_z;
    public float itemSize;
    private int _durability;
    public int durability
    {
        get
        {
            return _durability;
        }
        set
        {
            _durability = value;
            UnityEngine.Debug.Log($"Weapon Durability {_durability}");
            if (_durability <= 0)
            {
                MainWeaponAttachment = null;
            }
        }

    }

    private string character_r_hand_name;
    private string character_l_hand_name;

    // Start is called before the first frame update
    void Start()
    {
        // Character Hand Name formation
        string character_name = this.gameObject.name;
        //current_character = GameObject.Find(character_name);
        character_r_hand_name = GetComponentName(this.transform, "_r_hand01");
        character_l_hand_name = GetComponentName(this.transform, "_l_hand01");

        print(transform.name);
        print(GetComponentName(this.transform, "_r_hand01"));

    }

    // Update is called once per frame
    void Update()
    {
        //Check if weapon Exists
        if (MainWeaponAttachment != null)
        {
            //Create an instance of the weapon
            if (WeaponSlot1 == null)
            {
                WeaponSlot1 = Instantiate(MainWeaponAttachment, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            } else if (!WeaponSlot1.name.Contains(MainWeaponAttachment.name))
            {
                Destroy(WeaponSlot1);
                WeaponSlot1 = Instantiate(MainWeaponAttachment, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            }

            //WeaponSlot1 = Instantiate(MainWeaponAttachment, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;

            if (isDebug)
            {
                //Attach the weapon to the desired bone or submesh make sure its a child to the player object
                WeaponSlot1.transform.parent = GameObject.Find(character_r_hand_name).transform;
                //Set the desired position for the weapon
                WeaponSlot1.transform.localPosition = new Vector3(position_x, position_y, position_z);
                //set the desired rotation for the weapon
                WeaponSlot1.transform.localEulerAngles = new Vector3(rotation_x, rotation_y, rotation_z);
                //set item size
                WeaponSlot1.transform.localScale = new Vector3(itemSize, itemSize, itemSize);
            }
            else if (GameMaster.GM.weapon_mounting_dict.ContainsKey(MainWeaponAttachment.name))
            {
                //Attach the weapon to the desired bone or submesh make sure its a child to the player object
                WeaponSlot1.transform.parent = GameObject.Find(character_r_hand_name).transform;
                //Set the desired position for the weapon
                WeaponSlot1.transform.localPosition = new Vector3(
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].pos_x,
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].pos_y,
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].pos_z
                    );
                //set the desired rotation for the weapon
                WeaponSlot1.transform.localEulerAngles = new Vector3(
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].rot_x,
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].rot_y,
                    GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].rot_z
                    );
                //set item size
                var _itemSize = GameMaster.GM.weapon_mounting_dict[MainWeaponAttachment.name].itemSize;
                WeaponSlot1.transform.localScale = new Vector3(_itemSize, _itemSize, _itemSize);
            }
            else
            {
                // Do nothing
            }
        }
        else
        {
            // Destroy Holding Item
            Destroy(WeaponSlot1);
        }
    }

    string GetComponentName(Transform parent_transform, string game_obj_name_sub_string)
    {
        

        foreach (Transform child_transform in parent_transform)
        {
            if (child_transform.name.Contains(game_obj_name_sub_string)) return "/" + parent_transform.name + "/" + child_transform.name;
            string ret_val = GetComponentName(child_transform, game_obj_name_sub_string);
            if (ret_val != null) return "/" + parent_transform.name + ret_val;
        }

        return null;
    }
}
