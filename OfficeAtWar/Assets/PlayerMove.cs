﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private string horizontalInputName;
    [SerializeField] private string verticalInputName;

    [SerializeField] private float walkSpeed, runSpeed;
    [SerializeField] private float runBuildUpSpeed;
    [SerializeField] private KeyCode runKey;

    private float movementSpeed;

    [SerializeField] private float slopeForce;
    [SerializeField] private float slopeForceRayLength;

    private CharacterController charController;

    [SerializeField] private AnimationCurve jumpFallOff;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private Animator anima;
    [SerializeField] private GameObject objectCreator;
    private bool isJumping;
    public LayerMask AttackLayer;
    private PlayerHitPoint playerHPSystem;
    private void Awake()
    {
        charController = GetComponent<CharacterController>();
        playerHPSystem = GetComponent<PlayerHitPoint>();
    }
    private void Update()
    {
        if (!playerHPSystem.isDead)
        {
            PlayerMovement();
            UpdateAnimation();
        }
    }
    private void UpdateAnimation()
    {
        Debug.DrawRay(transform.position + Vector3.up * 1.15167f, transform.forward * 2.0f, Color.cyan);

        anima.SetFloat("vertical",Input.GetAxis(verticalInputName));
        anima.SetFloat("horizontal",Input.GetAxis(horizontalInputName));

        if(Input.GetKeyUp(KeyCode.LeftControl))
        {
            anima.Play("throw", 0);
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            anima.Play("RightPunch", 0);

            if (Physics.SphereCast(origin: transform.position + Vector3.up * 1.15167f,
                radius: 0.5f,
                direction: transform.forward,
                hitInfo: out RaycastHit hitinfo,
                maxDistance: 2.0f,
                layerMask: AttackLayer))
            {
                Debug.Log("attacked sth! " + hitinfo.collider.tag);
                if (hitinfo.collider.tag == "Enemy")
                {
                    if (GetComponent<ItemsOnHand>().MainWeaponAttachment != null)
                        GetComponent<ItemsOnHand>().durability -= 1;
                    hitinfo.collider.GetComponent<EnemyHitPointComponent>().TakeDamage();
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            anima.Play("jump",0);
        }

        if(Input.GetKey(KeyCode.G))
        {
            anima.Play("throw",0);
        }

        // fansy play
        if(Input.GetKey(KeyCode.L))
        {
            anima.Play("dance",0);
        }
    }
    private void PlayerMovement()
    {
        float horizInput = Input.GetAxis(horizontalInputName);
        float vertInput = Input.GetAxis(verticalInputName);

        Vector3 forwardMovement = transform.forward * vertInput;
        Vector3 rightMovement = transform.right * horizInput;


        charController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement, 1.0f) * movementSpeed);

        if ((vertInput != 0 || horizInput != 0) && OnSlope())
            charController.Move(Vector3.down * charController.height / 2 * slopeForce * Time.deltaTime);


        SetMovementSpeed();
        JumpInput();
    }

    private void SetMovementSpeed()
    {
        if (Input.GetKey(runKey))
            movementSpeed = Mathf.Lerp(movementSpeed, runSpeed, Time.deltaTime * runBuildUpSpeed);
        else
            movementSpeed = Mathf.Lerp(movementSpeed, walkSpeed, Time.deltaTime * runBuildUpSpeed);
    }


    private bool OnSlope()
    {
        if (isJumping)
            return false;

        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, charController.height / 2 * slopeForceRayLength))
            if (hit.normal != Vector3.up)
            {
                print("OnSlope");
                return true;
            }

        return false;
    }

    private void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }


    private IEnumerator JumpEvent()
    {
        charController.slopeLimit = 90.0f;
        float timeInAir = 0.0f;
        do
        {
            float jumpForce = jumpFallOff.Evaluate(timeInAir);
            charController.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;
            yield return null;
        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);

        charController.slopeLimit = 45.0f;
        isJumping = false;
    }

    public AudioClip kickSound;
    public AudioClip stepSound;
    public AudioClip hurtSound;
    public AudioClip throwSound;

    public void playKickSound()
    {
        AudioSource.PlayClipAtPoint(kickSound, transform.position, 2f);
    }
    public void playStepSound()
    {
        AudioSource.PlayClipAtPoint(stepSound, transform.position, 2f);
    }
    public void playHurtSound()
    {
        AudioSource.PlayClipAtPoint(hurtSound, transform.position, 2f);
    }
    public void playThrowSound()
    {
        AudioSource.PlayClipAtPoint(throwSound, transform.position, 2f);
    }
}