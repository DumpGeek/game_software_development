﻿using Invector.vCharacterController;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    public Transform destination;

    NavMeshAgent nmagent;
    Animator animator;
    new Rigidbody rigidbody;

    private EnemyHitPointComponent component;
    private EnemyAttack attack;
    private int state;
    public float scanRange = 2.0F;

    // Start is called before the first frame update
    void Start()
    {
        nmagent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
        rigidbody = GetComponent<Rigidbody>();

        component = GetComponent<EnemyHitPointComponent>();
        attack = GetComponent<EnemyAttack>();
    }

    void FixedUpdate()
    {
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player;
        Vector3 heading;
        // only follow target when HP > 0
        if (component.lifePoint > 0)
        {
            nmagent.SetDestination(destination.position);
            if (rigidbody.velocity.magnitude > 1.1 && (destination.position - transform.position).magnitude > 1.1f * nmagent.stoppingDistance)
            {
                animator.SetBool(vAnimatorParameters.IsSprinting, true);
                animator.SetFloat(vAnimatorParameters.InputMagnitude, 3, 0.2f, Time.deltaTime);
            }
            else
            {
                animator.SetBool(vAnimatorParameters.IsSprinting, false);
                animator.SetFloat(vAnimatorParameters.InputMagnitude, 0, 0.2f, Time.deltaTime);
            }

            if (state == 0) 
            {
                player = GameObject.FindGameObjectWithTag("Player");
                heading = player.transform.position - transform.position;
                if (heading.sqrMagnitude < scanRange * scanRange)
                {
                    //state = 1;
                    //nmagent.isStopped = true;
                    attack.EnemyAttackAnimation();
                    //Debug.Log("Enemy Attack");
                }
            }
        }

    }

    private void OnCollisionEnter(Collision otherObj)
    {
        //if (otherObj.collider.tag == "Attack")
        //{
        //    Debug.Log("hit the Enemy!");
        //    lifePoint -= 10;
        //    if (lifePoint <= 0)
        //    {
        //        Destroy(gameObject, 0.5F);
        //    }
        //}
    }
    
    public AudioClip kickSound;
    public AudioClip stepSound;
    public AudioClip hurtSound;
    public AudioClip throwSound;

    public void playKickSound()
    {
        AudioSource.PlayClipAtPoint(kickSound, transform.position, 2f);
    }
    public void playStepSound()
    {
        AudioSource.PlayClipAtPoint(stepSound, transform.position, 2f);
    }
    public void playHurtSound()
    {
        AudioSource.PlayClipAtPoint(hurtSound, transform.position, 2f);
    }
    public void playThrowSound()
    {
        AudioSource.PlayClipAtPoint(throwSound, transform.position, 2f);
    }
}
