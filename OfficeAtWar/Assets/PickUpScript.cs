﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PickUpScript : MonoBehaviour
{
    public AudioClip pickUpSounnd;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(GameMaster.GM.PlayerCharacter.transform.position + Vector3.up * 1.15167f, transform.forward * 2.2f);
        if (Input.GetKeyUp(KeyCode.Mouse1) && GameMaster.GM.PlayerCharacter.GetComponent<ItemsOnHand>().MainWeaponAttachment == null)
        {
            if (Physics.SphereCast(origin: transform.position + Vector3.up * 1.15167f,
                radius: 1.2f,
                direction: transform.forward,
                hitInfo: out RaycastHit x,
                maxDistance: 2.2f,
                layerMask: LayerMask.GetMask("Pickable")))
            {
                var gameObj = x.collider.gameObject;
                var temp = x.collider.GetComponent<MeshFilter>().sharedMesh.name;
                Debug.Log($"Pickable found {temp}");
                if (pickUpSounnd != null) AudioSource.PlayClipAtPoint(pickUpSounnd, transform.position);
                if (GameMaster.GM.weapon_mounting_dict.ContainsKey(temp))
                {
                    GameMaster.GM.PlayerCharacter.GetComponent<ItemsOnHand>().MainWeaponAttachment = GameMaster.GM.weapon_fbx_dict[temp];
                    if (GameMaster.GM.damaged_goods.ContainsKey(gameObj))
                    {
                        GameMaster.GM.PlayerCharacter.GetComponent<ItemsOnHand>().durability = GameMaster.GM.damaged_goods[gameObj];
                        GameMaster.GM.damaged_goods.Remove(gameObj);
                    }
                    else
                    {
                        GameMaster.GM.PlayerCharacter.GetComponent<ItemsOnHand>().durability = GameMaster.GM.weapon_durability_dict[temp];
                    }
                    Destroy(gameObj);
                }
            }
        }
    }
}
