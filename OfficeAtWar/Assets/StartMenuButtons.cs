﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuButtons : MonoBehaviour
{
    public string NextLevel = "Level01";

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(Screen.width/2 -50, Screen.height * 7/8 -50, 100, 100));

        if (GUILayout.Button("START"))
        {
            print("START BUTTON PRESSED");
            SceneManager.LoadScene(NextLevel);
        }

        if (GUILayout.Button("QUIT"))
        {
            print("QUIT BUTTON PRESSED");
            Application.Quit();
        }

        GUILayout.EndArea();
    }

}
