﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLevel : MonoBehaviour
{
    public string NextLevel = null;
    public string EnemyTagName = "Enemy";
    private GameObject[] EnemyList;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyList = GameObject.FindGameObjectsWithTag(EnemyTagName);


        if (EnemyList.Length == 0)
        {
            if (NextLevel != null) 
            {
                SceneManager.LoadScene(NextLevel);
            }
            // else
            // {
            //     Debug.Log("Win Process");
            //     StartCoroutine(winProcess());
            // }      
        }
    }
//     private IEnumerator winProcess()
//     {
//         //charaAnimator.Play("die", 0);
//          Debug.Log("Go Win Display");
//         yield return new WaitForSeconds(1);
//         GetComponent<START>().Win();
//         //Destroy(gameObject);
//     }     
}
