﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
//using UnityEngine.SceneManagement;
//using UnityEngine.UI;



public class PauseGame : MonoBehaviour
{
    private bool pause_on_off = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause_on_off = !pause_on_off;
        }

        if (pause_on_off)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;


        }
    }

    void OnGUI() {
        {
            if (pause_on_off)
            {
                // Menu
                GUILayout.BeginArea(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
                GUILayout.Label("GAME PAUSED");
                GUILayout.EndArea();
            }
        } }
}
