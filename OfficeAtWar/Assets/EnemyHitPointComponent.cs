﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitPointComponent : MonoBehaviour
{
    public int lifePoint = 30;
    public int deadEnemy;
    public bool isDead = false;
    public Animator charaAnimator;
    // Start is called before the first frame update
    void Start()
    {
        deadEnemy = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (lifePoint<0 && !isDead)
        {
            isDead = true;
            deadEnemy += 1;
            StartCoroutine(deathProcess());
        }    
    }

    private IEnumerator deathProcess()
    {
        charaAnimator.SetTrigger("isDead");
        yield return new WaitForSeconds (5);
        Destroy(gameObject);
    }

    private void playBeingHitAnimation()
    {
        var randomHitAnimation = Random.Range(1, 4);
        var hitAnimationId = "isHit_1";
        switch (randomHitAnimation)
        {
            case 1:
                {
                    hitAnimationId = "isHit_1";
                    break;
                }
            case 2:
                {
                    hitAnimationId = "isHit_2";
                    break;
                }
            case 3:
                {
                    hitAnimationId = "isHit_3";
                    break;
                }
            case 4:
                {
                    hitAnimationId = "isHit_4";
                    break;
                }
        }
        charaAnimator.SetTrigger(hitAnimationId);
    }

    private void HealthPoint_Damage()
    {
        lifePoint -= 10;
        Debug.Log("Enemy Life Point - 10");
    }

    public void TakeDamage()
    {
        Debug.Log("Enemy Taking Damage!");
        playBeingHitAnimation();
        HealthPoint_Damage();

        // GetComponent<Rigidbody>().AddForce(gameObject.transform.forward * -5f);
    }
    private void OnCollisionEnter(Collision otherObj)
    {
        //Debug.Log("Enemy Collision!");
        if (otherObj.collider.tag == "Attack")
        {
            TakeDamage();
        }
    }   
}
