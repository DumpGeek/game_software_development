﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitPoint : MonoBehaviour
{
    public Animator charaAnimator;
    public int PlayerLife { get; private set; } = 100;
    public bool isDead = false;
    public bool isGettingHurt = false;
    public float gettingHurtInterval = 1.5f;
    //GameObject enemy;
    private GameObject[] EnemyList;
    public string EnemyTagName = "Enemy"; 
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerLife <= 0 && !isDead)
        {
            // only play 1 shot
            isDead = true;
            StartCoroutine(deathProcess());
        }
        else if (PlayerLife > 0)
        {
           EnemyList = GameObject.FindGameObjectsWithTag(EnemyTagName);
            if (EnemyList.Length == 0)
            {  
                Debug.Log("No Enemy");
                StartCoroutine(winProcess());
            }
        }        
    }
    private IEnumerator deathProcess()
    {
        charaAnimator.Play("die", 0);
        yield return new WaitForSeconds(1);
        GetComponent<START>().GameOver();
        //Destroy(gameObject);
    }
    private IEnumerator winProcess()
    {
        //charaAnimator.Play("die", 0);
        yield return new WaitForSeconds(1);
        GetComponent<START>().Win();
        //Destroy(gameObject);
    }    
    private void HealthPoint_Damage()
    {
        PlayerLife -= 10;
        Debug.Log("Player HP: " + PlayerLife);
    }

    public void TakeDamage()
    {
        Debug.Log("Player Taking Damage!");
        if (!isGettingHurt & !isDead)
        {
            isGettingHurt = true;
            StartCoroutine(playBeingHitAnimation());
        }
    }
    private IEnumerator playBeingHitAnimation()
    {
        var randomHitAnimation = Random.Range(0, 3);
        var hitAnimationId = string.Format("getHurtAni_{0:D}", randomHitAnimation);
        Debug.Log(hitAnimationId);
        charaAnimator.Play(hitAnimationId, 0);
        yield return new WaitForSeconds(gettingHurtInterval);
        HealthPoint_Damage();
        isGettingHurt = false;
    }
}