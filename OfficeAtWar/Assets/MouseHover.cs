﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.EventSystems;

/**
public class MouseHover : MonoBehaviour
{

    public Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = Color.white;
    }


    void OnMouseEnter()
    {
        GetComponent<Renderer>().material.color = Color.yellow;
        print("Mouse Entered");
    }

    void OnMouseExit()
    {
        GetComponent<Renderer>().material.color = Color.white;
    }



    bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (IsMouseOverUI())
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.white;
        }          
    }

}
**/

public class MouseHover : MonoBehaviour, UnityEngine.EventSystems.IPointerEnterHandler
{
    public void OnPointerEnter(PointerEventData data)
    {
        Debug.Log("OnPointerEnter called.");
    }
}