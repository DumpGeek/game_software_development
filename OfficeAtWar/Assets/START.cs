﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class START : MonoBehaviour
{
    public GameObject uiObject;
    public GameObject uiObject2;
    public GameObject uiimage;
    public GameObject uiimage2;
    public GameObject uiimage3;
    private int state = 0;
    private PlayerHitPoint playerHPSystem;
    public bool buseImage;
    // Start is called before the first frame update
    void Start()
    {
        buseImage = true;
        if (!buseImage)
        {
            if (uiObject != null) uiObject.SetActive(true);
            if (uiObject2 != null) uiObject2.SetActive(false);
            if (uiimage != null) uiimage.SetActive(false);
            if (uiimage2 != null) uiimage2.SetActive(false);
            if (uiimage3 != null) uiimage3.SetActive(false);               
        }
        else
        {
            if (uiObject != null) uiObject.SetActive(false);
            if (uiObject2 != null) uiObject2.SetActive(false);
            if (uiimage != null) uiimage.SetActive(true);
            if (uiimage2 != null) uiimage2.SetActive(false);
            if (uiimage3 != null) uiimage3.SetActive(false);   
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (buseImage)
        {
            StartCoroutine(OnTriggerStartImage());
        }
        else
        {
            StartCoroutine(OnTriggerStart());
        }

    }

    private IEnumerator OnTriggerStart()
    {
        //if (other.tag == "Player")
        //{
        //    uiObject.SetActive(true);
        //}
        if (state == 0)
        {
            yield return new WaitForSeconds(3);
            //Debug.Log("Kill Start Text");
            uiObject.SetActive(false);
            //Destroy(uiObject);   
            state = 1;         
        }
    }
    private IEnumerator OnTriggerStartImage()
    {
        //if (other.tag == "Player")
        //{
        //    uiObject.SetActive(true);
        //}
        if (state == 0)
        {
            yield return new WaitForSeconds(3);
            //Debug.Log("Kill Start Text");
            uiimage.SetActive(false);
            //Destroy(uiObject);   
            state = 1;         
        }
    }
    private IEnumerator OnTriggerGameOver()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2);
        uiObject2.SetActive(true);
        //Destroy(uiObject);
    }

    private IEnumerator OnTriggerGameOverImage()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2);
        uiimage2.SetActive(true);
        //Destroy(uiObject);
    }

    private IEnumerator OnTriggerWinImage()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1);
        uiimage3.SetActive(true);
        //Destroy(uiObject);
    }

    public void GameOver()
    {
        Debug.Log("Game Over!");
        if (buseImage)
        {
            StartCoroutine(OnTriggerGameOverImage());
        }
        else
        {
            StartCoroutine(OnTriggerGameOver());
        }

    }
    public void Win()
    {
        Debug.Log("You are the boss!");
        StartCoroutine(OnTriggerWinImage());
    }  
}
