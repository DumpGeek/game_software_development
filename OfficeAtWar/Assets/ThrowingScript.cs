﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ThrowingScript : MonoBehaviour
{
    [Header("self Delete time")]
	public float selfDeleteTime = 10.0f;
	private float minimumForce = 500.0f;
	private float maximumForce = 1000.0f;
	private float throwForce;

	private void Awake () 
	{
		throwForce = Random.Range(minimumForce, maximumForce);
		GetComponent<Rigidbody>().AddRelativeTorque(Random.Range(500, 1500),Random.Range(0,0),Random.Range(0,0) * Time.deltaTime * 100);
	}

	private void Start () 
	{
		GetComponent<Rigidbody>().AddForce(gameObject.transform.forward * throwForce);

		StartCoroutine (afterThrowAction());
	}
	private void OnCollisionEnter (Collision collision) 
	{
		// impactSound.Play ();
	}
	private IEnumerator afterThrowAction() 
	{
		//Wait set amount of time
		yield return new WaitForSeconds(selfDeleteTime);

		//Raycast downwards to check ground
		RaycastHit checkGround;
		if (Physics.Raycast(transform.position, Vector3.down, out checkGround, 50))
		{
			Debug.Log("hit the ground!");
		}
		if (GameMaster.GM.damaged_goods[gameObject] < 0) Destroy(gameObject);
	}
}
