﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAttackScrip : MonoBehaviour
{
	//public class ThrowableOjbect
	//{
	//	public Transform objectType;
	//	public int quantity;
	//}
	//public ThrowableOjbect objectOnHand;
	//public ThrowableOjbect objectOnHandImage;
    public Transform objectCreationPoint;
    public float objectCreateDelay = 1.5f;
	public int durabilityDamage = 5;
    private bool isThrowing = false;
	private ItemsOnHand itemsOnHand;

    // Start is called before the first frame update
    void Start()
    {
		itemsOnHand = GameMaster.GM.PlayerCharacter.GetComponent<ItemsOnHand>();
	}
    // Update is called once per frame
    void Update()
    {
		if (Input.GetKey(KeyCode.G) && !isThrowing && itemsOnHand.MainWeaponAttachment != null) 
		{
			isThrowing = true;
			//objectOnHand.quantity--;
			StartCoroutine (throwObjectOnHand());
			//Play throw action at the same time on other script

			//if (objectOnHand.quantity <= 0)
			//{
			//	objectOnHand = null;
			//}

			//Debug.Log("weapon remain "+objectOnHand.quantity);
		}
    }
    private IEnumerator throwObjectOnHand () {
		//Wait for set amount of time before creating item 0
		yield return new WaitForSeconds (objectCreateDelay);
		//Greate item 0 at object appear point
		var x = GameMaster.GM.weapon_throwprefab_dict[itemsOnHand.MainWeaponAttachment.name];
		var good = Instantiate(x, objectCreationPoint.transform.position, objectCreationPoint.transform.rotation);
		good.layer = LayerMask.NameToLayer("Pickable");
		GameMaster.GM.damaged_goods.Add(good, itemsOnHand.durability - durabilityDamage);
		itemsOnHand.MainWeaponAttachment = null;

		isThrowing = false;
	}

	//public void setObjectOnHandById(Transform objectId, int quantity)
	//{
	//	if (objectOnHand==null)
	//	{
	//		objectOnHand = new ThrowableOjbect();
	//	}
		
	//	objectOnHand.objectType = objectId;
	//	objectOnHand.quantity = 100;
	//}

	//public void reloadWeapon()
	//{
	//	objectOnHand = new ThrowableOjbect();
	//	objectOnHand = objectOnHandImage;
	//}
}
