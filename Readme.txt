For updating things to the git master branch
	git add -A
	git commit -m “Description of Changes that you made”
	git push origin HEAD:master

For getting changes from other (your change will be covered)
	git pull -f

For merge changes to your local branch
	git merge remotename/branchname
	Merges updates made online with your local work
